#pragma once

#include "types.h"
#include "picture.h"

typedef struct {
    union {
        struct {
            Picture *tl;
            Picture *tr;
            Picture *bl;
            Picture *br;
        };
        Picture *pics[4];
    };
    struct int2 offset;
    struct int2 size;
} Field;

void Field_init(Field *f, int w, int h);

void Field_scroll(Field *f);

void Field_resize(Field *f, int w, int h);

void Field_draw(Field *f, SDL_Renderer *rend);
