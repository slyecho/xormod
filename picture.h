#pragma once

#include <stdint.h>
#include <SDL3/SDL.h>

typedef struct {
    int x;
    int y;
    int w;
    int h;
    int s;
    SDL_Surface *surface;
    SDL_Texture *texture;
    uint32_t *pixels;
    uint32_t color;
    int dirty;
} Picture;

void Picture_render(Picture *p, SDL_Renderer *rend);
Picture *Picture_new(int x, int y, int w, int h, uint32_t color);
void Picture_delete(Picture *p);

void Picture_render(Picture *p, SDL_Renderer *rend);
void Picture_move(Picture *p, int x, int y);
