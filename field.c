
#include "field.h"
#include "picture.h"
#include <SDL3/SDL.h>

#define SWAP(a, b) ({ \
    __typeof(a) tmp = (a); \
    (a) = (b); \
    (b) = tmp; \
})

void Field_init(Field *f, int w, int h) {
    f->tl = Picture_new(0, 0, w, h, 0xffffffff /*0xff7fffff*/);
    f->tr = Picture_new(w, 0, w, h, 0xffffffff /*0xffff7fff*/);
    f->bl = Picture_new(0, h, w, h, 0xffffffff /*0xffffff7f*/);
    f->br = Picture_new(w, h, w, h, 0xffffffff /*0xff7fff7f*/);
    f->offset = (struct int2) { 0,  0 };
    f->size = (struct int2) { w, h };
}

void Field_scroll(Field *f) {
    int w = f->size.x;
    int h = f->size.y;
    if (f->offset.y < f->tl->y) {
        SWAP(f->tl, f->bl);
        SWAP(f->tr, f->br);
        Picture_move(f->tl, 0, -2*h);
        Picture_move(f->tr, 0, -2*h);
    }
    if (f->offset.x < f->tl->x) {
        SWAP(f->tl, f->tr);
        SWAP(f->bl, f->br);
        Picture_move(f->tl, -2*w, 0);
        Picture_move(f->bl, -2*w, 0);
    }
    if (f->offset.y >= f->br->y) {
        SWAP(f->tl, f->bl);
        SWAP(f->tr, f->br);
        Picture_move(f->bl, 0, 2*h);
        Picture_move(f->br, 0, 2*h);
    }
    if (f->offset.x >= f->br->x) {
        SWAP(f->tl, f->tr);
        SWAP(f->bl, f->br);
        Picture_move(f->tr, 2*w, 0);
        Picture_move(f->br, 2*w, 0);
    }
}

void Field_resize(Field *f, int w, int h) {

    w = (w + 7) / 8 * 8;
    h = (h + 7) / 8 * 8;

    Picture_delete(f->tl);
    Picture_delete(f->tr);
    Picture_delete(f->bl);
    Picture_delete(f->br);

    f->tl = Picture_new(0, 0, w, h, 0xffffffff /*0xff7fffff*/);
    f->tr = Picture_new(w, 0, w, h, 0xffffffff /*0xffff7fff*/);
    f->bl = Picture_new(0, h, w, h, 0xffffffff /*0xffffff7f*/);
    f->br = Picture_new(w, h, w, h, 0xffffffff /*0xff7fff7f*/);

    f->size = (struct int2) { w, h };
}

void Field_draw(Field *f, SDL_Renderer *rend) {
    for (int i = 0; i < 4; i++) {
        Picture *p = f->pics[i];
        Picture_render(p, rend);

        SDL_FRect dst = {
            p->x - f->offset.x, p->y - f->offset.y, p->w, p->h,
        };
        SDL_RenderTexture(rend, p->texture, NULL, &dst);
    }
}

