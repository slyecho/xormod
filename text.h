#pragma once

#include <SDL3/SDL.h>

#define RC(r, c) (c)*font_w, (r)*font_h

void draw_text(SDL_Renderer *renderer,
    char *text, size_t len, int x, int y, uint8_t r, uint8_t g, uint8_t b);
