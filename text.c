#include "font.h"

#include <SDL3/SDL.h>

static SDL_Texture *font = NULL;

void draw_text(SDL_Renderer *renderer,
    char *text, size_t len, int x, int y, uint8_t r, uint8_t g, uint8_t b) {

    if (font == NULL) {
        SDL_Surface *s = SDL_LoadBMP_IO(SDL_IOFromConstMem(font_data, font_data_len), true);
        SDL_SetSurfaceColorKey(s, true, SDL_MapSurfaceRGB(s, 255, 0, 255));
        font = SDL_CreateTextureFromSurface(renderer, s);
        SDL_DestroySurface(s);
    }

    SDL_SetTextureColorMod(font, r, g, b);    
    SDL_FRect src = { .w = font_w, .h = font_h },
             dst = { .x = x, .y = y, .w = font_w, .h = font_h };
    for (size_t i = 0; i < len; i++) {
        unsigned char c = text[i];
        if (c == 0) break;
        if (c == '\n') {
            dst.x = x;
            dst.y += font_h;
            continue;
        }
        src.x = (c & 0xf) * font_w;
        src.y = (c >> 4) * font_h;
        SDL_RenderTexture(renderer, font, &src, &dst);
        dst.x += font_w;
    }
    
}
