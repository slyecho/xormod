#include <stdio.h>
#include <stdlib.h>

#include "picture.h"
#include "SDL3/SDL_render.h"

Picture *Picture_new(int x, int y, int w, int h, uint32_t color) {
    
    Picture *p = calloc(1, sizeof(Picture));

    p->x = x;
    p->y = y;
    p->w = w;
    p->h = h;
    p->color = color;
    p->s = (w * sizeof(uint32_t) + 7) / 8 * 8;
    p->dirty = 1;

    p->pixels = malloc(p->s * h);
    p->surface = SDL_CreateSurfaceFrom(w, h, SDL_PIXELFORMAT_RGBA32, p->pixels, p->s);
    if (p->surface == NULL) {
        fprintf(stderr, "Can't create surface: %s\n", SDL_GetError());
        exit(-1);
    }
    return p;
}

void Picture_delete(Picture *p) {
    if (p->texture != NULL) SDL_DestroyTexture(p->texture);
    SDL_DestroySurface(p->surface);
    free(p->pixels);
    free(p);
}

typedef uint32_t uint32_8 __attribute__ ((vector_size (8 * sizeof(uint32_t))));

void Picture_render(Picture *p, SDL_Renderer *rend) {
    if (!p->dirty) return;

    int w = p->w, h = p->h;
    uint32_8 c = {
        p->color, p->color, p->color, p->color,
        p->color, p->color, p->color, p->color
    };
    const uint32_8 y0 = { 0, 0, 0, 0, 0, 0, 0, 0 };
    const uint32_8 x0 = { 0, 1, 2, 3, 4, 5, 6, 7 };
    uint32_8 y = y0 + p->y;
    for (int j = 0, r = 0; j < h; j++, y += 1) {
        uint32_8 x = x0 + p->x;
        for (int i = 0; i < w; i += 8, r += 8, x += 8) {
            uint32_8 z = (x ^ y) % 9;
            //uint32_8 z = (-((~(x & y)) + (~(-y)))) % 7;
            uint32_8 f = (z!=0) & c;
            memcpy(&p->pixels[r], (uint32_t*)&f, sizeof(f));
        }
    }
    if (p->texture) SDL_DestroyTexture(p->texture);
    p->texture = SDL_CreateTextureFromSurface(rend, p->surface);
    if (p->texture == NULL) {
        fprintf(stderr, "Can't create texture: %s\n", SDL_GetError());
        exit(-1);
    }
    SDL_SetTextureBlendMode(p->texture, SDL_BLENDMODE_NONE);
    p->dirty = 0;
}

void Picture_move(Picture *p, int x, int y) {
    p->x += x;
    p->y += y;
    p->dirty = 1;
}

