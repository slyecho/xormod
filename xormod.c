#include "font.h"
#include "field.h"
#include "text.h"
#include "picture.h"
#include "platform.h"
#include "types.h"

#include <stdio.h>
#include <inttypes.h>
#include <SDL3/SDL.h>

int main() {

    int w = 1280, h = 720;

    if (!SDL_Init(SDL_INIT_VIDEO)) {
        fprintf(stderr, "Can't init: %s\n", SDL_GetError());
        return -1;
    }

    SDL_Window *window = SDL_CreateWindow("xormod", w, h, ACCEL_PLATFORM | SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        fprintf(stderr, "Can't open window: %s\n", SDL_GetError());
        return -1;
    }

    SDL_GetWindowSize(window, &w, &h);

    SDL_Renderer *rend = SDL_CreateRenderer(window, NULL);
    
    Field f;
    Field_init(&f, w, h);

    int dragging = 0;
    Uint64 last = SDL_GetTicks();

    int running = 1;
    int fullscreen = 0;
    int vsync = 0;
    double avg_delta = 0;

    SDL_SetRenderVSync(rend, vsync);

#ifdef NDEBUG
    int debug = 0;
#else
    int debug = 1;
#endif

    while (running) {
        Uint64 ticks = SDL_GetTicks();
        Uint64 delta = ticks - last;
        last = ticks;
        avg_delta = 0.75 * avg_delta + 0.25 * (double) delta;
        int speed = (int)(16 * avg_delta / 16.667); // 16 pixels per update @ 60 fps

        const bool *keys = SDL_GetKeyboardState(NULL);
        if (keys[SDL_SCANCODE_LEFT] || keys[SDL_SCANCODE_A]) {
            f.offset.x -= speed;
        }
        if (keys[SDL_SCANCODE_RIGHT] || keys[SDL_SCANCODE_D]) {
            f.offset.x += speed;
        }
        if (keys[SDL_SCANCODE_UP] || keys[SDL_SCANCODE_W]) {
            f.offset.y -= speed;
        }
        if (keys[SDL_SCANCODE_DOWN] || keys[SDL_SCANCODE_S]) {
            f.offset.y += speed;
        }

        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_EVENT_QUIT: running = 0; break;
                case SDL_EVENT_WINDOW_RESIZED: {
                    w = event.window.data1;
                    h = event.window.data2;
                    Field_resize(&f, w, h);
                    break;
                }
                case SDL_EVENT_KEY_DOWN: {
                    if (event.key.repeat) break;
                    switch (event.key.key) {
                        case SDLK_ESCAPE:
                        case SDLK_Q: running = 0; break;
                        case SDLK_G: debug ^= 1; break;
                        case SDLK_F: SDL_SetWindowFullscreen(window, fullscreen ^= 1); break;
                        case SDLK_V: SDL_SetRenderVSync(rend, vsync ^= 1); break;
                    }
                    SDL_HideCursor();
                    break;
                }
                case SDL_EVENT_MOUSE_BUTTON_DOWN:
                case SDL_EVENT_MOUSE_BUTTON_UP: {
                    dragging = event.button.down;
                    break;
                }
                case SDL_EVENT_MOUSE_MOTION: {
                    if (dragging) {
                        f.offset.x -= event.motion.xrel;
                        f.offset.y -= event.motion.yrel;
                    } else {
                        SDL_ShowCursor();
                    }
                    break;
                }
            }
        }

        Field_scroll(&f);
        Field_draw(&f, rend);

        if (debug) {
            char buf[72];
            SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
            SDL_RenderFillRect(rend, &(SDL_FRect) {
                0, 0, sizeof(buf)*font_w, 8*font_h
            });

            draw_text(rend, ACCEL_PLATFORM_STR, sizeof(ACCEL_PLATFORM_STR), RC(1,1), 255, 255, 255);

            size_t len = snprintf(buf, sizeof(buf),
                "off.x: %d, off.y: %d, vsync: %s, \xEBT: %.3f, %.1fFPS, speed: %02d",
                    f.offset.x, f.offset.y, vsync ? "ON" : "OFF", avg_delta, 1000.0 / avg_delta, speed);
            draw_text(rend, buf, len, RC(2, 1), 255, 255, 255);

            char *q[] = {"TL", "TR", "BL", "BR"};
            for (int i = 0; i < 4; i++) {
                Picture *p = f.pics[i];
                len = snprintf(buf, sizeof(buf), "%s \xB3 x: %05d, y: %05d, w: %d, h: %d, p: %p",
                               q[i], p->x, p->y, p->w, p->h, p);
                draw_text(rend, buf, len, RC(3+i, 1), 255, 255, 255);
            }
        }

        SDL_RenderPresent(rend);

        if (!vsync && avg_delta < 8.0) {
            SDL_Delay(8.0 - avg_delta);
        }
    }

    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
